<?php

/**
 * @file
 * Adds the back_2_top module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_page_attachments().
 */
function back_2_top_page_attachments(array &$attachments)
{
    $config = \Drupal::config('back_2_top.settings');
    $settings = $config->get();
    $button_settings = [
        'back_2_top_scroll_speed' => $settings['back_2_top_scroll_speed'],
    ];

    $attachments['#attached']['library'][] = 'back_2_top/back_2_top_js';
    $attachments['#attached']['library'][] = 'back_2_top/back_2_top_css';

    $css = '';

    if ($settings['back_2_top_position'] == 'bottom-right') {
        $css = 'right: 20px; bottom: 20px;';
    } elseif ($settings['back_2_top_position'] == 'bottom-left') {
        $css = 'left: 20px; bottom: 20px;';
    } elseif ($settings['back_2_top_position'] == 'bottom-center') {
        $css = 'left: 50%; bottom: 20px; transform: translateX(-50%);';
    }

    if ($settings['back_2_top_bottom_offset'] != 20) {
        $css .= 'margin-bottom: ' . $settings['back_2_top_bottom_offset'] . 'px;';
    }

    if ($css != '') {
        $attachments['#attached']['html_head'][] = [
            [
                '#tag' => 'style',
                '#value' => '#toTop { ' . $css . ' }',
            ],
            'css',
        ];
    }

    $attachments['#attached']['drupalSettings']['back_2_top'] = $button_settings;
}

/**
 * Implements hook_help().
 */
function back_2_top_help($route_name, RouteMatchInterface $route_match)
{
    switch ($route_name) {
        case 'help.page.back_2_top':
            return '<p>This module adds a back to top button to the bottom of the page.</p>';
    }
}

/**
 * Implements hook_uninstall().
 */
function back_2_top_uninstall($is_syncing)
{
    // Delete remaining general module variables.
    \Drupal::state()->delete('back_2_top.settings');
}
