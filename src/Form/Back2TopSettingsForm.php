<?php

namespace Drupal\back_2_top\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class Back2TopSettingsForm
 *
 * @package Drupal\back_2_top\Form
 */
class Back2TopSettingsForm extends ConfigFormBase
{

    /**
     * {@inheritDoc}
     *
     */
    public function getFormId()
    {
        return 'back_2_top.settings';
    }

    /**
     * {@inheritDoc}
     */
    protected function getEditableConfigNames()
    {
        return [
            'back_2_top.settings',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('back_2_top.settings');
        $settings = $config->get();

        $form['#attached']['library'][] = 'back_2_top/back_2_top';

        $form['back_2_top.settings'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Back to Top Settings'),
        ];

        $form['back_2_top.settings']['back_2_top_position'] = [
            '#type' => 'select',
            '#title' => $this->t('Back-2-Top Button Position'),
            '#options' => [
                'bottom-right' => $this->t('Bottom Right'),
                'bottom-left' => $this->t('Bottom Left'),
                'bottom-center' => $this->t('Bottom Center'),
            ],
            '#default_value' => $config->get('back_2_top_position'),
        ];

        $form['back_2_top.settings']['back_2_top_scroll_speed'] = [
            '#type' => 'textfield',
            '#size' => '6',
            '#maxlength' => '6',
            '#title' => $this->t('Back-2-Top Scroll Speed'),
            '#default_value' => $config->get('back_2_top_scroll_speed'),
        ];

        $form['back_2_top.settings']['back_2_top_bottom_offset'] = [
            '#type' => 'textfield',
            '#size' => '6',
            '#maxlength' => '6',
            '#title' => $this->t('Back-2-Top Bottom Offset'),
            '#default_value' => $config->get('back_2_top_bottom_offset'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->configFactory->getEditable('back_2_top.settings');
        $form_values = $form_state->getValues();
        $config->set('back_2_top_position', $form_values['back_2_top_position'])
            ->set('back_2_top_scroll_speed', $form_values['back_2_top_scroll_speed'])
            ->set('back_2_top_bottom_offset', $form_values['back_2_top_bottom_offset'])
            ->save();
        parent::submitForm($form, $form_state);

        drupal_flush_all_caches();
    }
}
