# BACK 2 TOP

A configurable button that appears at the bottom of web page, that appears when users start to scroll down, then brings users back to the top of the page when it is clicked.

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Uninstall
 * Maintainers


## INTRODUCTIONS


This module creates a back-to-top button on the bottom-right corner of your website. The button will initially be
invisible, but once the viewer scrolls down the button will appear. The webpage will scroll back up slowly once the
button is clicked. This comes in handy especially when you have a very long webpage, to keep users from doing the
tedious task of having to scroll all the way back up using their mouse scroll button.

What this module does is it adds a new div to your website with an i.d. of 'toTop'. You can add custom styling to
the button by accessing #toTop in your CSS stylesheet. To customize it you may need to add an '!important' rule
(without quoutes) next to your CSS attribute to override the modules default theme. For example, to change the
button's color you need to create a new color rule for the #toTop i.d. in your templates css stylesheet, then add
'!important' at the end to make sure it overrides all other CSS rules.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/back_2_top

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/back_2_top


## REQUIREMENTS

No special requirements.


## INSTALLATION

 * Install as you would normally a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


  ### POST-INSTALL CONFIGURATION

  1. Navigate to 'Structure > Block layout' of your site
  2. Scroll to the bottom most region of your block layout page, which is often the footer region, and click the 'Place block' button 
  3. A pop up modal appears and this module's block should be among the list of available blocks, click the 'Place block' button next to it 
  4. List which pages of your site should have this block, adding '/*' (without quotes) will make it show on all pages, click save
  5. Save your changes to the 'Block layout' page
  6. You can now go to '/admin/config/user-interface/back-2-top' to configure your button, click 'Save configuration'
  6. To see the changes you may need to clear your site's cache by going to '/admin/config/development/performance'
 
## UNINSTALL
 
 1. Go to '/admin/modules/uninstall'
 2. Check the box next to the module name
 3. Click the 'Uninstall' button
 4. If installed using composer, in the Drupal site's root dir execute 'composer remove drupal/back_2_top'.
 

## MAINTAINERS

Current maintainers:
 * Joseph Bernadas (jbernadas) - https://www.drupal.org/u/jbernadas
