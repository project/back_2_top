

(function () {
  Drupal.behaviors.back_2_top = {

    /* Attach context, settings - for Ajax call to back_2_top.module */
    attach(context, settings) {

      document.addEventListener('DOMContentLoaded', function (drupalSettings) {

        /* Select the "toTop" button element */
        var toTopButton = document.getElementById('toTop');

        /* Initially hide the toTop button */
        toTopButton.style.display = 'none';

        /* Add scroll event listener to window */
        window.addEventListener('scroll', function () {

          /* Check scroll position */
          if (window.scrollY >= 40) {
            /* If scrolled down enough, fade in the toTop button */
            toTopButton.style.display = 'block';
          } else {
            /* Otherwise, fade out the toTop button */
            toTopButton.style.display = 'none';
          }
        });

        var scrollSpeed = settings.back_2_top.back_2_top_scroll_speed;
        /* Add click event listener to toTop button */
        toTopButton.addEventListener('click', function (event) {
          /* Prevent default anchor behavior */
          event.preventDefault();

          /* Smooth scroll to top of page */
          /* Get current scroll position */
          var scrollStep = -window.scrollY / (scrollSpeed / 15); /* 15ms per frame for smooth scroll */

          var scrollInterval = setInterval(function () {
            if (window.scrollY !== 0) {
              window.scrollBy(0, scrollStep);
            } else {
              clearInterval(scrollInterval);
            }
          }, 15);
        });
      });
    }
  };
})(Drupal);
